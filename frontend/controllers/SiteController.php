<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model = $this->checkContact();
        $aCats = new \common\models\Work();
        $aCats = $aCats->getCategories();
        $aWork = \common\models\Work::find()->andWhere(['active' => 1])->orderBy(['category_id' => SORT_DESC, 'id' => SORT_DESC])->all();
        return $this->render('index', ['model' => $model, 'aWork' => $aWork, 'aCategories' => $aCats]);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    private function checkContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            if ( $model->validate() ) {
                if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                    Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
                } else {
                    Yii::$app->session->setFlash('danger', 'There was an error sending your message.');
                }
            }
        }
        return $model;
    }

    /**
     * Displays privacy page.
     *
     * @return mixed
     */
    public function actionPrivacy()
    {
        return $this->render('privacy');
    }
}

<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.css',
        'css/all.css',
        'css/animate.min.css',
        'css/icofont.css',
        'css/responsive.css',
        'css/style.css',
    ];
    public $js = [
        'js/jquery.min.js',
        'js/jquery.easing.1.3.js',
        'js/popper.min.js',
        'js/bootstrap.min.js',
        'js/wow.min.js',
        'js/jquery.waypoints.js',
        'js/jquery.counterup.min.js',
        'js/custom.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}

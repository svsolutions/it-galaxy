<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
?>
 <!--MAIN BANNER AREA START -->
    <div class="banner-area banner-3">
        <div class="overlay dark-overlay"></div>
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="row">
                      <div class="col-lg-8 text-left col-sm-12 col-md-12" style="margin: 0px;">
                            <div class="banner-content content-padding">
                                <h1 class="banner-title">Everything you need to grow your business.</h1>
                                <p><?= Yii::$app->name ?> provides BPO service and solutions that can fit your technology needs. We can help you reach your goals and implement your projects without effort. </p>
                                <a href="#service-head" class="btn btn-white btn-circled">lets start</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--MAIN HEADER AREA END -->
  <!--  SERVICE PARTNER START  -->
    <section id="service-head" class="bg-feature">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 m-auto">
                    <div class="section-heading text-white">
                        <h4 class="section-title" style="color:#fff; padding-bottom: 10px;">Our services are based on deep industry knowledge</h4>
                        <p style="font-size: 18px;">We aim to provide each client with custom-made services specifically designed for their needs and requirements.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  SERVICE PARTNER END  -->
 <!--  SERVICE AREA START  -->
    <section id="service" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box">
                        <div class="service-img-icon">
                            <img src="<?= Url::to(['/img/icon/007-digital-marketing-3.png']); ?>" alt="service-icon" class="img-fluid">
                        </div>
                        <div class="service-inner">
                            <h4>BPO</h4>
                            <p>We offer a full range of innovative and cost-effective business process outsourcing<span> (BPO) services</span> to help you focus on your primary goals while we are taking care of the rest.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box ">
                        <div class="service-img-icon">
                            <img src="<?= Url::to(['/img/icon/008-digital-marketing-2.png']); ?>" alt="service-icon" class="img-fluid">
                        </div>
                        <div class="service-inner">
                            <h4>IT outsourcing</h4>
                            <p>Address complex business requirements with our full-cycle <span>application design, development and integration service</span>  using web and all the latest underlying technologies.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box">
                        <div class="service-img-icon">
                            <img src="<?= Url::to(['/img/icon/003-task.png']); ?>" alt="service-icon" class="img-fluid">
                        </div>
                        <div class="service-inner">
                            <h4>Digital Marketing </h4>
                            <p><?= Yii::$app->name ?> provides creativity, tactics, analytics and complete solution for <span>online branding and marketing </span> that helps in increasing conversions.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-6 col-md-6" style="margin: 0 0 0 auto">
                    <div class="service-box">
                        <div class="service-img-icon">
                            <img src="<?= Url::to(['/img/icon/010-digital-marketing.png']); ?>" alt="service-icon" class="img-fluid">
                        </div>
                        <div class="service-inner" >
                            <h4>Global Support and Customer Care</h4>
                            <p><?= Yii::$app->name ?> provides a <span>multilingual customer support services</span> including inbound and outbound customer inquiries, order processing, loyalty programs and service activations.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6" style="margin: 0 auto 0 0;">
                    <div class="service-box">
                        <div class="service-img-icon">
                            <img src="<?= Url::to(['/img/icon/006-analytics.png']); ?>" alt="service-icon" class="img-fluid">
                        </div>
                        <div class="service-inner">
                            <h4>Call Center Outsourcing Services </h4>
                            <p>We can provide your customers with <span>an expert, personalized service</span> - making every interaction count. Drive sales with effective lead management.</p>
                        </div>
                    </div>
                </div>
                            
            </div>
        </div>
    </section>
    <!--  SERVICE AREA END  -->
    <!--  ABOUT AREA START  -->
    <section id="intro" class="section-padding">
        <div style="background-color: rgba(255,255,255,0.3);"> 
        <div class="row">
                <div class="col-lg-12 col-sm-12 m-auto"> 
                    <div class="section-heading" style="color:#000;">
                        <h5 class="section-title" style="padding-top: 40px"><?= Yii::$app->name ?> has developed proven methods to meet<br> your core objectives, and to provide ongoing support.</h5>
                    </div>
                </div>
            </div>
            
            <div class="row">
            <div class="intro-bg-img d-none d-lg-block d-md-block"></div>
            <div class="col-lg-5  d-none d-lg-block col-sm-12" style="max-width: 21%!important;">
            </div> 
                                   
                
                
                <div class="col-lg-7 col-md-12 col-sm-12">
                    <div class="row"  style="padding-bottom: 20px!important;">
                    <div class="row">
                      <div class="col-lg-12">
                           <div class="intro-cta">
                               <p class="lead"><b> Take advantage of the partnership with <?= Yii::$app->name ?>:<b></p>
                           </div>
                        </div>
                    </div>
                     <ul class="about-list" style="margin-top: 0px!important;">
                           <li><b>Benefits</b><br>Uncover new revenue opportunities.</li>
                           <li><b>Flexibility</b><br>Enhancement in customer understanding.</li>
                           <li><b>Expertise</b><br>Efficient management of customers.</li>
                           <li><b>Cost Savings</b><br>Improving efficiency and reducing costs.</li>
                           <li><b>Value</b><br>Process reengineering for better results.</li>
                           <li><b>Motivation</b><br>Implementation of global quality standards and initiatives.</li>
                        </ul>
                      
                    </div>
                 
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  ABOUT AREA END  -->

    <!--  SERVICE AREA START  -->
    <section id="about" class="bg-light" style="background-color:#fff!important;">
        <div class="about-bg-img d-none d-lg-block d-md-block"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-sm-12 col-md-8">
                    <div class="about-content">
                      <h5 class="subtitle" style="padding-bottom: 20px;">About us</h5>
                         <div class="line" style="margin:0px;"></div>
                        <h3><?= Yii::$app->name ?> operates as a BPO and technology provider to companies from various areas of the online and e-commerce field. </h3>
                        <p>We offer professional solutions in the sphere of IT, marketing, as well as support and direct sales. Our culture allows our team to quickly solve problems and get results for our clients. By clearly understanding the client’s needs and striving to achieve the outlined goal, we can assure that our partnership is aligned in the most efficient way.</p>
                        </div>
                </div>
            </div>
        </div>
    </section>
    <!--  SERVICE AREA END  -->

    
 
    <!--  COUNTER AREA START  -->
    <section id="counter" class="section-padding">
        <div class="overlay dark-overlay"></div>
        <div class="container">
            <div class="row" >
                <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="counter-stat">
                        <i class="icofont icofont-heart"></i>
                        <span class="counter">600</span>
                        <h5>Our Happy Clients</h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="counter-stat">
                        <i class="icofont icofont-rocket"></i>
                        <span class="counter">258</span>
                        <h5>Projects Done</h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="counter-stat">
                        <i class="icofont icofont-hand-power"></i>
                        <span class="counter">420</span>
                        <h5>Experienced staff</h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="counter-stat">
                        <i class="icofont icofont-shield-alt"></i>
                        <span class="counter">45</span>
                        <h5>Ongoing Projects</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  COUNTER AREA END  -->
        

    <!--  BLOG AREA START  -->
    <section id="blog" class="section-padding">
        <div class="container" style="max-width: 100%; padding:0px;"  >
            <div class="row" style="margin: 0px auto;">
                <div class="col-lg-12 col-sm-12 m-auto">
                    <div class="section-heading">
                        <h5 class="section-title "style="padding-top: 40px;">Our office</h5>
                        <div class="line"></div>
                        <p class="office" >Our Sofia office are currently home to over 90 employees, mastering in more than 10 languages. If you are looking for an average sized company that can give you all the benefits and facilities of a modern working place, while retaining the sense of familiarity and bond of a small community of people, we are the place for you.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-sm-6 col-md-4" style="padding:0px!important;">
                    <div class="blog-block ">
                         <div class="item"><img src="<?= Url::to(['/img/blog/office_1111.png']); ?>" alt="" class="img-fluid"></div>
                        <div class="blog-text">
                        </div>
                    </div>
                </div>
                 <div class="col-lg-4 col-sm-6 col-md-4" style="padding:0px!important;">
                    <div class="blog-block ">
                         <div class="item"><img src="<?= Url::to(['/img/blog/office_22.png']); ?>" alt="" class="img-fluid"></div>
                    <div class="blog-text">
                    </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-4" style="padding:0px!important;">
                    <div class="blog-block ">
                        <div class="item"><img src="<?= Url::to(['/img/blog/office_3333.png']); ?>" alt="" class="img-fluid"></div>
                        <div class="blog-text">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-4" style="padding:0px!important;">
                    <div class="blog-block">
                       <div class="item"><img src="<?= Url::to(['/img/blog/office_44.png']); ?>" alt="" class="img-fluid"> </div>
                        <div class="blog-text">
                        </div>
                    </div>
                </div>
                 <div class="col-lg-4 col-sm-6 col-md-4" style="padding:0px!important;">
                    <div class="blog-block ">
                         <div class="item"><img src="<?= Url::to(['/img/blog/office_5555.png']); ?>" alt="" class="img-fluid"></div>
                        <div class="blog-text">
                        </div>
                    </div>
                </div>
                 <div class="col-lg-4 col-sm-6 col-md-4" style="padding:0px!important;">
                    <div class="blog-block ">
                        <div class="item"><img src="<?= Url::to(['/img/blog/office_66.png']); ?>" alt="" class="img-fluid"></div>
                        <div class="blog-text">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  BLOG AREA END  -->
    
      <!--  CARRERS AREA START  -->
    <section id="careers" class="bg-light" style="background-color:#fff!important;">
        <div class="carrer-bg-img d-none d-lg-block d-md-block"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-sm-12 col-md-8" style="margin-bottom: 100px!important;">
                    <div class="about-content">
                        <h5 class="subtitle" style="padding-bottom: 20px;">Careers</h5>
                        <div class="line" style="margin:0px;"></div>
                        <h3>Powered By Our People</h3>
                        <p>We are a fast growing, multicultural environment with a dynamic, fun loving team of creative passionate employees. Our greatest asset is our people. Our success is based on our people. </p>
                        <p style="padding-bottom: 20px;">Ready for new challenges?<br>
                        Please check the currently available positions.</p>
                        </div>
                        <!-- <a href="#" class="btn btn-white btn-circled">Search jobs</a> -->
                </div>
            </div>
        </div>
    </section>
    <!--  CARRERS AREA END  -->
    <!--  Available Positions START  -->
      <section id="positions" style="background-color:#3c5782!important;    padding-bottom: 100px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-sm-12 col-md-8" style="margin-bottom: 50px!important;">
                    <div class="about-content">
                        <h5 class="subtitle" style="padding-bottom: 20px;color: #fff!important;">Available Positions</h5>
                        <div class="line" style="margin:0px;"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php $bNext = true; ?>
                <?php foreach ($aCategories  as $k => $pRow ) : ?>
                <div class="col-md-3 mb-5">
                    <div class="btn-group">
                        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?= $pRow; ?>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                        <?php foreach ( $aWork[0]->getCategoryItems($k) as $pTmp ) : ?>
                          <a onclick="return false;" href="<?= $pTmp->link; ?>" target="_blank" class="dropdown-item" type="button"><?= $pTmp->title; ?></a>
                      <?php endforeach; ?>
                        </div>
                      </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <!--  Available Positions END  -->
    <!--  PARTNER START  -->
    <?= $this->render('contact', ['model' => $model]); ?>
    <!--  CONTACT END  -->
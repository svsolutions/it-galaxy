<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

?>

<section id="contact" class="section-padding bg-main">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-mfd-12">
                <div class="section-heading">
                    <h4 class="section-title" style="padding-top: 40px;">Get in touch</h4>
                     <div class="line"></div>
                    <p style="padding-bottom: 20px;">If you have any questions, please do not hesitate to send us a message. </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-sm-12 m-auto">
                <div class="contact-form ">
                    <?php $form = ActiveForm::begin(['id' => 'contact-form', 'options' => ['class' => 'contact__form'], 'action' => Url::to(['/#contact'])]); ?>

                        <div class="row">
                            <div class="col-12">
                        <?php foreach (Yii::$app->session->getAllFlashes() as $type => $message): ?>
                            <?php if (in_array($type, ['success', 'danger', 'warning', 'info'])): ?>
                                <div class="alert alert-<?= $type;?> contact__msg" role="alert"><?= $message; ?></div>
                            <?php endif ?>
                        <?php endforeach ?>
                            </div>
                        </div>

                        <div class="row">

                            <?= $form->field($model, 'name', ['options' => ['class' => 'col-md-6 mb-4']]) ?>

                            <?= $form->field($model, 'email', ['options' => ['class' => 'col-md-6 mb-4']])->textInput() ?>

                            <?= $form->field($model, 'subject', ['options' => ['class' => 'col-md-12 mb-4']])->textInput() ?>

                            <?= $form->field($model, 'body', ['options' => ['class' => 'col-md-12 mb-4']])->textarea(['rows' => 6]) ?>

                            <?= $form->field($model, 'verifyCode', ['options' => ['class' => 'col-md-12 mb-4']])->widget(Captcha::className(), [
                                'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-9">{input}</div></div>',
                            ]) ?>

                            <div class="col-12 mt-5 text-center">
                                <input name="submit" type="submit" class=" btn btn-hero btn-circled" value="Send Message">
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

                


          

            
        </div>
    </div>

</div>

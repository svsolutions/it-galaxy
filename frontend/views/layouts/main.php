<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Galaxy IT Solutions is a business process outsourcing provider. We are the expert partner for your enterprise growth."> 
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,600,700|Rubik:400,600,700" />
    <meta name="keywords" content="digital marketing, business process outsourcing(BPO) provider, IT outsourcing, global support, call center outsourcing">

    <link rel="icon" href="<?= Url::to(['/img/favicon.png']) ?>">

    <?= Html::csrfMetaTags() ?>
    
    <title><?= Html::encode($this->title) ?></title>
    
    <?php $this->head() ?>
</head>
<body data-spy="scroll" data-target=".fixed-top">
<?php $this->beginBody() ?>


    <nav class="navbar navbar-expand-lg fixed-top trans-navigation">
        <div class="container">
            <a class="navbar-brand" href="<?= Url::to(['/']); ?>">
                <img src="<?= Url::to(['/img/logo.png']); ?>" alt="<?= Yii::$app->name ?>" class="img-fluid b-logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNav" aria-controls="mainNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon">
                    <i class="fa fa-bars"></i>
                </span>
              </button>

            <div class="collapse navbar-collapse justify-content-end" id="mainNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link smoth-scroll" href="<?= Url::to(['/']) ?>">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link smoth-scroll" href="<?= Url::to(['/']) ?>#service-head">Service</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link smoth-scroll" href="<?= Url::to(['/']) ?>#intro">Benefits</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link smoth-scroll" href="<?= Url::to(['/']) ?>#about">About</a>
                    </li>
                            
                    <li class="nav-item">
                        <a class="nav-link smoth-scroll" href="<?= Url::to(['/']) ?>#blog">Office</a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link smoth-scroll" href="<?= Url::to(['/']) ?>#careers">Careers</a>
                    </li>
                                            
                    <li class="nav-item">
                        <a class="nav-link smoth-scroll" href="<?= Url::to(['/']) ?>#contact">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>





        <?= $content ?>

    <!--  FOOTER AREA START  -->
    
     <section id="footer" class="section-padding">
        <div class="container">
                     <div class="row">
                <div class="col-lg-12 text-center">
                <div class="footer-copy">
                    <a href="<?= Url::to(['/']) ?>" style="font-size:12px; color:#fff; padding-bottom: 10px; text-decoration: underline;">Home</a> | 
                    <a href="<?= Url::to(['/site/privacy']) ?>" style="font-size:12px; color:#fff; padding-bottom: 10px; text-decoration: underline;">Privacy Policy </a>
                </div>
                
                    <div class="footer-copy" style="padding-top: 10px;">
                        © <?= date('Y') . ' ' . Yii::$app->name; ?>. All Rights Reserved.
                    </div>
                    
                </div>
            </div>
        </div>
    </section> 
    <!--  FOOTER AREA END  -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

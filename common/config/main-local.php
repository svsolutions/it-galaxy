<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=abcglobal',
            'username' => 'root',
            'password' => 'qwerty',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,

            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'leo.superhosting.bg', 
                'username' => 'office@it-galaxy.com',
                'password' => '@-office-@', 
                'port' => '25', // '26',
                'encryption' => 'tls',
                // 'streamOptions' => [ 
                //     'ssl' => [ 
                //         'allow_self_signed' => true,
                //         'verify_peer' => false,
                //         'verify_peer_name' => false,
                //     ],
                // ]
            ],
        ],
    ],
];

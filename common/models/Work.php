<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "work".
 *
 * @property int $id
 * @property int $category_id
 * @property string $title
 * @property string $link
 * @property int $active
 */
class Work extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'work';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'active'], 'integer'],
            [['title', 'link'], 'required'],
            [['title'], 'string', 'max' => 512],
            [['link'], 'string', 'max' => 1024],
        ];
    }

    public function getCategories($nId = '') {
        $aData = [
            '1' => 'Marketing Department',
            '2' => 'Finance Department',
            '3' => 'IT Department',
            '4' => 'Customer Support',
            // '5' => 'Technical Support Consultant with English',
            // '6' => 'Technical Support Consultant with German',
            // '7' => 'Support Account Manager with English',
            // '8' => 'Support Account Manager with German',
            // '9' => 'Digital Marketing specialist with German and English'
        ];

        if ( $nId && isset($aData[$nId]) ) {
            return $aData[$nId];
        }

        return $aData;
    }

    public function getCategoryItems($nId) {
        return Work::find()->andWhere(['category_id' => $nId])->all();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Категория'),
            'title' => Yii::t('app', 'Заглавие'),
            'link' => Yii::t('app', 'Линк'),
            'active' => Yii::t('app', 'Активност'),
        ];
    }
}

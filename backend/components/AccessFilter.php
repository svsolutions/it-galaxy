<?php

namespace backend\components;

use Yii;

/**
 * AccessFilter
 *
 * @author Marin Vasilev <m.vasilev@evoapp.bg>
 * @link <http://evoapp.bg> EvoApp Ltd. Official Website
 */
class AccessFilter extends \yii\filters\AccessRule
{
    /** @inheritdoc */
    protected function matchRole($user)
    {
        if (empty($this->roles)) {
            return true;
        }

        foreach ($this->roles as $role) {
            if ($role === '?') {
                if (Yii::$app->user->isGuest) {
                    return true;
                }
            } elseif ($role === '@') {
                if (!Yii::$app->user->isGuest) {
                    return true;
                }
            } elseif ($role === 'moderator') {
                if (!Yii::$app->user->isGuest && Yii::$app->user->can('moderator')) {
                    return true;
                }
            } elseif ($role === 'user') {
                if (!Yii::$app->user->isGuest && Yii::$app->user->can('user')) {
                    return true;
                }
            }
        }

        return false;
    }
}
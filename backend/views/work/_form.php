<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Work */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="work-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <div class="row">
	    <?= $form->field($model, 'title', ['options' => ['class' => 'col-md-6']])->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'category_id', ['options' => ['class' => 'col-md-4']])->dropdownList($model->getCategories()) ?>

	    <?= $form->field($model, 'active', ['options' => ['class' => 'col-md-2']])->dropdownList(['1' => Yii::t('app', 'Да'), '0' => Yii::t('app', 'Не')]) ?>
	</div>
    
	<div class="row">
	    <?= $form->field($model, 'link', ['options' => ['class' => 'col-md-6']])->textInput(['maxlength' => true]) ?>

	    <div class="form-group col-md-6">
	    	<label>&nbsp;</label>
	    	<br />
	        <?= Html::submitButton('Запази', ['class' => 'btn btn-success col-md-6']) ?>
	    </div>
	</div>

    <?php ActiveForm::end(); ?>

</div>
